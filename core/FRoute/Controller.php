<?php

use FRouter\RedirectFactory;

class Controller extends \FRouter\FRouter
{
	public function beforeFilter($key, array $attr = [])
	{
		if(preg_match('/^\w+\|\w+$/', $key))
        {   
            $filters = explode('|',$key);
            foreach ($filters as $filter) {
                $app = App::make($filter);
                if($app instanceof RedirectFactory)
                    return $app->redirect();
                else
                    print $app;
            }
        }
        else
        {
            $app = App::make($key);
            if($app instanceof RedirectFactory)
                return $app->redirect();
            else
                print $app;               
        }
	}
}