<?php namespace EDb;

use PDO;

abstract class MysqlBuilder
{
	protected $from;
	protected $select = "*";
	protected $skip = 0;
	protected $take = 0;
	protected $debug = false;
	protected $sets = [];
	protected $wheres = [];
	protected $orWheres = [];
	protected $joins = [];
	protected $orderBy = [];
	protected $groupBy = [];
	protected $having = [];
	protected $orHaving = [];
	protected $joinModels = [];

	public function sets(array $sets = [])
	{
		$this->sets = $sets;
		return $this;
	}

	protected function connecting()
	{
		try {
			if($this->connection == null)
			{
			    $this->connection = new PDO('mysql:host='.$this->host.';port='.$this->port.';dbname='.$this->database, $this->user, $this->password,$this->options);
			    $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
		} catch(PDOException $e) {
		    echo 'ERROR: ' . $e->getMessage();
		}
		return $this;
	}

	protected function query($sql,array $map = [])
	{
		try {
			$conn = $this->connection;
			$stmt = $conn->prepare($sql);
			if($this->debug) var_dump($stmt->queryString);
			$stmt->execute($map);		 
			return $stmt->fetchAll(PDO::FETCH_OBJ);
		} catch(PDOException $e) {
		    echo 'ERROR: ' . $e->getMessage();
		    return False;
		}

		return $this;
	}

	protected function inserting()
	{
		$sqls[] = "INSERT INTO " . $this->table . "(".implode(',',array_keys($this->sets)).")";
		$sqls[] = "VALUES(:".implode(',:',array_keys($this->sets)).")";
		$sql = implode(' ', $sqls);
		$pdo = $this->connection;
		try {
		  	$pdo->beginTransaction();
		  	$stmt = $pdo->prepare($sql);
			if($this->debug) var_dump($stmt->queryString);
			$stmt->execute($this->sets);
			$pdo->commit();
		} catch(PDOException $e) {
			$pdo->rollback();
		  	echo 'Error: ' . $e->getMessage();
		  	return False;
		}
		
		return $this;
	}

	protected function deleting()
	{
		$bindWheres = [];
		$sqls[] = "DELETE FROM " . $this->table;
		$sqls[] = $this->generateWheres();
		$sql = implode(' ', $sqls);
		$pdo = $this->connection;		 
		try {
			$pdo->beginTransaction();
		  	$stmt = $pdo->prepare($sql);
			if($this->debug) var_dump($stmt->queryString);
		  	$stmt->execute();
		    $pdo->commit();
		} catch(PDOException $e) {
			$pdo->rollback();
		  	echo 'Error: ' . $e->getMessage();
		}
		return $this;
	}

	private function needUpdate($original_data)
	{
		$sets = $this->sets;
		foreach ($sets as $key => $value) {
			if(!in_array($key,['created_by','created_at','updated_at','updated_by']))
			{
				if($value != $original_data->$key)
				{
					return true;
				}
			}
		}
		return false;
	}

	protected function updating()
	{
		$original_data = $this->selectForVersioning();
		if(count($original_data) > 0)
		{
			if(!$this->needUpdate($original_data[0]))
				return;
		}


		$bindWheres = [];
		$setValues = [];
		$sqls[] = "UPDATE " . $this->table;
		foreach($this->sets as $key => $val)
		{
			$sets[] = $key . ' = ?';
			$setValues[] = $val;
		}
		$sqls[] = "SET " . implode(',', $sets);
		$sqls[] = $this->generateWheres();
		$sql = implode(' ', $sqls);
		$pdo = $this->connection;	

		try {
		  	$pdo->beginTransaction();	 
		  	$stmt = $pdo->prepare($sql);
		  	$stmt->execute($setValues);
			if($this->debug) var_dump($stmt->queryString);
		  	$pdo->commit();
			$new_data = $this->selectForVersioning();
			$this->updateVersioning($original_data[0],$new_data[0]);
		} catch(PDOException $e) {
			$pdo->rollback();
		  	echo 'Error: ' . $e->getMessage();
		}

		return $this;
	}

	protected function generateWheres()
	{
		$WHERE = [];
		if(count($this->wheres) > 0)
		{
			$WHERE[] = implode(' AND ',$this->wheres);
		}
		if(count($this->orWheres) > 0)
		{
			$WHERE[] = implode('',$this->orWheres);
		}

		return count($WHERE)>0? "WHERE " . implode('',$WHERE):'';
	}

	protected function generateHavings()
	{
		$HAVING = [];
		if(count($this->having) > 0)
		{
			$HAVING[] = implode(' AND ',$this->having);
		}
		if(count($this->orHaving) > 0)
		{
			$HAVING[] = implode('',$this->orHaving);
		}

		return count($HAVING)>0? "HAVING " . implode('',$HAVING):'';
	}

	protected function joining()
	{
		if(count($this->joins) > 0)
			return implode(' ',$this->joins);
		return '';
	}

	protected function ordering()
	{
		if(count($this->orderBy) > 0)
			return "ORDER BY ". implode(', ',$this->orderBy);
		return '';
	}

	protected function grouping()
	{
		if(count($this->groupBy) > 0)
			return "GROUP BY ". implode(', ',$this->groupBy);
		return '';
	}

	protected function selectForVersioning()
	{
		if(in_array($this->table, ['sys_autoinc']))
			return;
		$sqls[] = "SELECT * FROM " . $this->table;
		$sqls[] = $this->generateWheres();
		$sql = implode(' ', $sqls);
		try {
			$conn = $this->connection;
			$stmt = $conn->prepare($sql);
			$stmt->execute();		 
			return $stmt->fetchAll(PDO::FETCH_OBJ);
		} catch(PDOException $e) {
		    echo 'ERROR: ' . $e->getMessage();
		    return False;
		}
	}

	protected function updateVersioning($original_data,$new_data)
	{
		if(in_array($this->table, ['sys_autoinc']))
			return;
		$arr = [];
		$arr[] = $id = uuid();
		$arr[] = $tbl = $this->table;
		$arr[] = $ref_id = str_replace('WHERE', '',$this->generateWheres());
		$arr[] = $original_data = json_encode($original_data);
		$arr[] = $new_data = json_encode($new_data);
		$arr[] = $created_at = date('Y-m-d H:i:s');
		$arr[] = $updated_at = date('Y-m-d H:i:s');
		$arr[] = $created_by = \Auth::check()? (Session::has('original_users_loginid')? Session::get('original_users_loginid'):\Auth::user()->loginid):'System';
		$arr[] = $updated_by = \Auth::check()? (Session::has('original_users_loginid')? Session::get('original_users_loginid'):\Auth::user()->loginid):'System';
		$sqls[] = "INSERT INTO table_versions (`id`, `table_name`, `table_reference_id`, `original_data`, `new_data`, `created_at`, `updated_at`, `created_by`, `updated_by`)";
		$sqls[] = "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
		$sql = implode(' ', $sqls);
		$pdo = $this->connection;
		try {
		  	$pdo->beginTransaction();
		  	$stmt = $pdo->prepare($sql);
			$stmt->execute($arr);
			$pdo->commit();
		} catch(PDOException $e) {
			$pdo->rollback();
		  	echo 'Error: ' . $e->getMessage();
		  	return False;
		}
	}
}

class MysqlNestedQuery
{
	protected $wheres = [];
	protected $orWheres = [];
	protected $joins = [];
	protected $orJoins = [];

	public function where($where, $operator = ' = ', $what='')
	{
		if(!preg_match('/^\d*\d$/i', $what) && !preg_match('/^\(.+\)$/i', $what))
				$what = '"' . $what. '"';
		$this->wheres[] = $where .' '. $operator .' '. $what;
		return $this;
	}

	public function orWhere($where, $operator = ' = ', $what)
	{
		if(!preg_match('/^\d*\d$/i', $what) && !preg_match('/^\(.+\)$/i', $what))
				$what = '"' . $what. '"';
		$this->orWheres[] = ' OR ' . $where .' '. $operator .' '. $what;
		return $this;
	}

	public function on($leftField, $operator = '=', $rightField = '')
	{
		$this->joins[] = "{$leftField} {$operator} {$rightField}";
		return $this;
	}

	public function orOn($leftField, $operator = '=', $rightField = '')
	{
		$this->orJoins[] = " OR {$leftField} {$operator} {$rightField}";
		return $this;
	}

	public function generateOns()
	{
		$JOINS = [];
		if(count($this->joins) > 0)
		{
			$JOINS[] = implode(' AND ',$this->joins);
		}
		if(count($this->orJoins) > 0)
		{
			$JOINS[] = implode(' ',$this->orJoins);
		}

		return count($JOINS)>0? '('. implode('',$JOINS) . ')':'';
	}

	public function generateWheres()
	{
		$WHERE = [];
		if(count($this->wheres) > 0)
		{
			$WHERE[] = implode(' AND ',$this->wheres);
		}
		if(count($this->orWheres) > 0)
		{
			$WHERE[] = implode('',$this->orWheres);
		}

		return count($WHERE)>0? '('. implode('',$WHERE) . ')':'';
	}

}