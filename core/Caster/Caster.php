<?php namespace Caster;

use \ReflectionObject;

class Caster 
{
	public function toObject($destination, $sourceObject)
	{
	    if (is_string($destination)) {
	        $destination = new $destination();
	    }
	    $sourceReflection = new ReflectionObject($sourceObject);
	    $destinationReflection = new ReflectionObject($destination);
	    $sourceProperties = $sourceReflection->getProperties();
	    foreach ($sourceProperties as $sourceProperty) {
	        $sourceProperty->setAccessible(true);
	        $name = $sourceProperty->getName();
	        $value = $sourceProperty->getValue($sourceObject);
	        if ($destinationReflection->hasProperty($name)) {
	            $propDest = $destinationReflection->getProperty($name);
	            $propDest->setAccessible(true);
	            $propDest->setValue($destination,$value);
	        } else {
	            $destination->$name = $value;
	        }
	    }
	    return $destination;
	}

	public function toObjectWithNullValue($destination, $sourceObject)
	{
	    if (is_string($destination)) {
	        $destination = new $destination();
	    }
	    $sourceReflection = new ReflectionObject($sourceObject);
	    $destinationReflection = new ReflectionObject($destination);
	    $sourceProperties = $sourceReflection->getProperties();
	    foreach ($sourceProperties as $sourceProperty) {
	        $sourceProperty->setAccessible(true);
	        $name = $sourceProperty->getName();
	        $value = null;
	        if ($destinationReflection->hasProperty($name)) {
	            $propDest = $destinationReflection->getProperty($name);
	            $propDest->setAccessible(true);
	            $propDest->setValue($destination,$value);
	        } else {
	            $destination->$name = $value;
	        }
	    }
	    return $destination;
	}

	public function toArray($object)
	{
	    $reflectionClass = new ReflectionClass(get_class($object));
	    $array = array();
	    foreach ($reflectionClass->getProperties() as $property) {
	        $property->setAccessible(true);
	        $array[$property->getName()] = $property->getValue($object);
	        $property->setAccessible(false);
	    }
	    return $array;
	}

	public function toJson($object)
	{
	    $array = [];
	    $reflection = new ReflectionClass($object);
	    foreach ($reflection->getProperties() as $property) {
	        $property->setAccessible(true);
	        $array[$property->getName()] = $property->getValue($object);
	    }
	    return json_encode($array);
	}
}