<?php namespace Conf;

class Config
{
	use \McCade\McCade;

	public function __construct()
	{
		$Conf = new ConfBuilder;
		$Conf->setConfigDirectory(__dir__ . '/../../app/config/');
		$this->load($Conf);
	}
}