<?php namespace evCache;

use closure,
    ArrayObject;

class evCacheBuilder
{

    protected $Cache;

    public function __construct(){
        $this->setup();
    }

    protected function setup(){
        $CacheFactory = "evCache\\".ucfirst(Config::get('cache.default')).'Factory';       
        $this->Cache = new $CacheFactory;
    }

    public function has($key){
        return $this->Cache->isCached($key);
    }

    public function get($key)
    {
        $result = $this->Cache->getCache($key);

        if(is_object($result))
        {
            $result = $result[0];
        }

        return $result;
    }

    public function put($key, $value, $ttl = 0){
        if(is_object($value))
            $value = new ArrayObject([$value]);
        return $this->Cache->setCache($key, $value, $ttl);
    }

    public function remember($key, $ttl, closure $closure)
    {
        if($this->has($key) && ($result = $this->get($key)))
        {
            return $result;
        }

        $value = call_user_func($closure);
        $this->put($key,$value,$ttl);
        return $value;
    }

    public function rememberForever($key, closure $closure)
    {
        return $this->remember($key, 0, $closure);
    }

    public function forget($key='')
    {
        return $this->Cache->clearCache($key);
    }

}
