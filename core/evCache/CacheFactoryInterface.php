<?php namespace evCache;

interface CacheFactoryInterface
{
	public function isCached($key);

    public function getCache($key);

    public function setCache($key, $value, $ttl = 0);

    public function clearCache($key = '');
}