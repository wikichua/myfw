<?php

class TestClass3
{
	function __construct(TestClass $Tests) {
		$this->Tests = $Tests;
	}

	function out()
	{
		return 'from TestClass3 calling TestClass '. $this->Tests->out();
	}
}

class TestClass {
	function out()
	{
		return 'I am from TestClass';
	}
}

class TestClass5 {
	function out()
	{
		return 'I am from TestClass5';
	}
}

class TestClass4
{
	function __construct(TestClass $Tests, TestClass2 $Testss) {
		$this->Tests = $Tests;
		$this->Testss = $Testss;
	}

	function out()
	{
		return $this->Tests->out() .' and from TestClass2 '. $this->Testss->out();
	}
}