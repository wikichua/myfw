<?php

class DependencyInjection
{
	function __construct() {
		$this->var = 'Dependency Injection';
	}

	function __toString()
	{
		return $this->var;
	}
}