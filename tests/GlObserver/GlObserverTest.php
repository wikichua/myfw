<?php namespace GlObserver;

require_once __dir__.'/../../app/bootstrap.php';

require_once 'Book.php';
require_once 'User.php';
require_once 'Library.php';
/**\
 * ObserverTest
 *
 */
class GlObserverTest extends \PHPUnit_Framework_TestCase
{
 	public function testInitiate()
 	{
 		$Subscriber = new Event;
 		$Subscriber->kill();

 		// attach or subscribe foo
 		$Subscriber->listen('foo', function($a='',$b=''){
 			echo 'notify from foo ' . $a .','. $b . "\n";
 		},[1,2]);

 		// attach or subscribe bar
 		$Subscriber->listen('bar', function($a='',$b=''){
 			echo 'notify from bar ' . $a .','. $b . "\n";
 		},['a','b']);

 		// attach or subscribe object testing as key
 		$testing = new testing;
 		$Subscriber->listen($testing,'testtest',['abc','123']);

 		$Subscriber->listen('testing','\\GlObserver\\testing@testtest'); // attach with different notify object function
		$Subscriber->fire('testing',['fuck','fucker']); // testing@testtest

		$Subscriber->fire(); // notify all
 		$Subscriber->fire($testing,['abc','123']); // notify just testing method
 		$Subscriber->fire('foo'); // notify just foo key
 		$Subscriber->fire(['bar','foo'],['abc','123']); // notify specific keys and by its order

 		$Subscriber->kill('foo'); // detach foo
 		$Subscriber->fire(); // as already detach foo, so only notify bar and testing

 		$Subscriber->kill($testing); // detach testing object
		$Subscriber->fire(); // only left bar in the observer

 	}
/*
 	public function testCanCallMagicSetterAndGetterInBook()
 	{
 		$Book = new \Book;
 		$Book->setTitle('PHP');
 		$this->assertSame('PHP', $Book->getTitle());
 	}

 	public function testUserCanBorrowBookFromLibrary()
 	{
 		$User = new \User(new \Book, 'Wiki');
 		$User->borrow('PHP','Wiki','Pai');

 		$Library = new \Library;
 		$Library->listen($User,'updateLibrary');

 		$Library->updateRecordBooks();
 	}

 	public function testNewBookAriveAndNotifyUser()
 	{
 		$Book = new \Book;
 		$Book->setTitle('PHP Design Patterns');
 		$Book->setAuthor('WikiChua');
 		$Book->setPublisher('PaiPai');

 		$Library = new \Library;
 		$Library->listen($Book);

 		$Library->newBookEntry();
 	}

 	public function testPutAllTogether()
 	{
 		$User = new \User(new \Book, 'Wiki');
 		$User->borrow('MySQL','Wiki','Pai');

 		$Book = new \Book;
 		$Book->setTitle('MySQL Design Patterns');
 		$Book->setAuthor('WikiChua');
 		$Book->setPublisher('PaiPai');

		$Library = new \Library;
 		$Library->listen($User,'updateLibrary');
 		$Library->listen($Book);
 		$Library->fire();

 	}

 	public function testPutAllTogetherWithMoreUsersAndNewBooks()
 	{
 		$User = new \User(new \Book, 'Wiki');
 		$User->borrow('MongoDB','Wiki','Pai');
 		$User1 = new \User(new \Book, 'Wiki');
 		$User1->borrow('Redis','Wiki','Pai');
 		$User2 = new \User(new \Book, 'Wiki');
 		$User2->borrow('Apache','Wiki','Pai');

 		$Book = new \Book;
 		$Book->setTitle('MongoDB Design Patterns');
 		$Book->setAuthor('WikiChua');
 		$Book->setPublisher('PaiPai');
 		$Book1 = new \Book;
 		$Book1->setTitle('Redis Design Patterns');
 		$Book1->setAuthor('WikiChua');
 		$Book1->setPublisher('PaiPai');
 		$Book2 = new \Book;
 		$Book2->setTitle('Apache Design Patterns');
 		$Book2->setAuthor('WikiChua');
 		$Book2->setPublisher('PaiPai');

		$Library = new \Library;
 		$Library->listen($User,'updateLibrary');
 		$Library->listen($User1,'updateLibrary');
 		$Library->listen($User2,'updateLibrary');
 		$Library->listen($Book);
 		$Library->listen($Book1);
 		$Library->listen($Book2);
 		$Library->fire();

 	}*/
}

class testing
{
	function testtest($a='',$b='')
	{
		echo 'notify from testing ' . $a .','. $b  . "\n";
	}
}