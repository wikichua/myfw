<?php
error_reporting(-1);
ini_set('display_errors', 1);

require_once __dir__.'/../../app/bootstrap.php';

class AuditTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers class::Log()
     */
    public function testLog()
    {
    	Audit::action('testing')->content('testing from phpunit')->log();
    }
}
