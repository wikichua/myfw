<?php 
foreach (glob(__dir__."/helpers/*.php") as $filename) {
    include_once $filename;
}

custom_class_alias(require(__dir__.'/alias.php'));

function custom_class_alias($aliases)
{
	$classes = array_values($aliases);
	$namespaces = [];
	$declared = [];
	foreach ($classes as $namespace) {
		$reflect = new \ReflectionClass($namespace);
		$namespaces[] = $reflect->getNamespaceName();
	}
	foreach ($aliases as $alias => $class) {
		if (!class_exists($alias)) {
			class_alias($class,$alias);
		}
		new $class;
		
		foreach ($namespaces as $namespace) {
			$a = $namespace."\\".$alias;
			if($a != $class && !in_array($a,$declared))
			{
				if (!class_exists($a)) {
					class_alias($class,$a);
				}
				$declared[] = $a;
			}
		}
	}
}

spl_autoload_register(function ($class) {
	$filename = __dir__.'/models/' . $class . '.php';
	if(is_file($filename))
	{
    	include_once $filename;
    	if (!class_exists("\\".$class)) {
    		class_alias($class,"\\".$class);
    	}
	}
});

spl_autoload_register(function ($class) {
	$filename = __dir__.'/controllers/' . $class . '.php';
	if(is_file($filename))
	{
    	include_once $filename;
    	if (!class_exists("\\".$class)) {
    		class_alias($class,"\\".$class);
    	}
	}
});

require_once __dir__.'/../core/Help/Helper.php';
require_once __dir__.'/filters.php';

/**
 * This particular route only for captcha
 */
Route::get('captcha',['as'=>'captcha','uses'=>function(){
	return Form::captchaImage();
}]);

/**
 * This particular route only for captcha
 */
Route::get('error/token',['as'=>'token.error','uses'=>function(){
	return "Invalid Token!";
}]);