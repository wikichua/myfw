<?php

return $aliases = [
	'Config' => "Conf\Config",
	'Cli' => "Prun\Cli",
	'App' => "DiLioc\App",
	'Event' => "GlObserver\Event",
	'Route' => "FRouter\Route",
	'Redirect' => "FRouter\Redirect",
	'Response' => "FRouter\Response",
	'View' => "Biew\View",
	'DB' => "EDb\DB",
	'Cast' => "Caster\Cast",
	'Session' => "SSion\Session",
	'Cache' => "evCache\Cache",
	'Form' => "Former\Form",
	'Mail' => "Mameil\Mail",
	'Validator' => "Vladitor\Validator",
	'Input' => "RInPut\Input",
	'Paginate' => "Paggy\Paginate",
	'Carbon' => "Carbon\Carbon",
];