<?php
// if your hostname contain dot, please replace dot to underscore
return [
	'wikichua' => [
		'database_connection' => 'mysql',
		'doc_root'=>'public',
		'upload_dir' => __DIR__ . '/../../uploads',
	],
];