Dear <?=$full_name?>,<br />
<br />
You are now registered with the e-FiT Online System. Your login information is as follows :<br />
<br />
Login ID : <?=$loginid; ?><br />
Password : <?=$password; ?><br />
<br />
You may now proceed to apply for a Feed-in Approval through the SEDA Portal at the following URL :<br />
<?=route('login')?><br />
If you did not request for this registration, please contact us at eFiT@seda.gov.my.<br />
<br />
<br />
Thank you.<br /><p>Sustainable Energy Development Authority Malaysia,<br />
    Galeria PjH, Aras 9, Jalan P4W,<br />
    Persiaran Perdana, Presint 4,<br />
    62100 Putrajaya,<br />
    Malaysia.<br /><br />
    Phone : +603 - 8870 5800<br />
    Fax : +603 - 8870 5900<br />
    Website : www.seda.gov.my<br />
    GPS : 2&deg;54\'45\"N  101&deg;41\'4\"E</p>