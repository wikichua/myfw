<?php

function getthehostname()
{
	return str_replace('.', '_', gethostname());
}

function toNumber($value)
{
	return number_format(str_replace(',', '', $value),2);
}