<?php

class AuthEventsHandler
{
	public function reCacheLoginInfo()
	{
		$id = Auth::user()->id;
		$User = Auth::find($id);
		if($User)
		{
			$User->login_token = md5(time());
			Session::put('loginid', $User->loginid);
			Session::put('login_token', $User->login_token);
			Cache::put($User->loginid,$User,60*60);
		}
	}
}